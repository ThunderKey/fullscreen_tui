# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fullscreen_tui/version'

Gem::Specification.new do |spec|
  spec.name          = 'fullscreen_tui'
  spec.version       = FullscreenTui::VERSION
  spec.authors       = ['Nicolas Ganz']
  spec.email         = ['nicolas@keltec.ch']

  spec.summary       = 'A simple fullscreen wrapper for terminal uis'
  spec.homepage      = 'https://gitlab.com/ThunderKey/fullscreen_tui'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'colorize', '~> 0.8'
  spec.add_dependency 'ruby-terminfo', '~> 0.1'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
end
