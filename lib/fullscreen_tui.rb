# frozen_string_literal: true

require 'terminfo'
require 'io/console'
require 'English'
require_relative 'fullscreen_tui/header'
require_relative 'fullscreen_tui/footer'
require_relative 'fullscreen_tui/renderer'

class FullscreenTui
  attr_accessor :selection, :lines
  attr_reader :header, :footer, :renderer
  def initialize headers:, renderer: nil, enable_jk: true
    @header = Header.new headers
    @footer = Footer.new
    @selection = 0
    @renderer = renderer || Renderer.new
    @up_actions = [:arrow_up, enable_jk ? 'k' : nil].reject(&:nil?)
    @down_actions = [:arrow_down, enable_jk ? 'j' : nil].reject(&:nil?)
  end

  def run
    catch :quit do
      loop do
        print_screen
        react
      end
    end
  end

  def print_screen
    clear

    rows, columns = TermInfo.screen_size

    header_text = header.output
    rows -= required_lines header_text, columns
    footer_text = footer.output width: columns, lines: lines.size, current_line: selection
    rows -= required_lines footer_text, columns

    renderer.header header_text unless header_text.empty?

    print_lines rows, columns

    renderer.footer footer_text
  end

  def key_press key
    case key
    when *@up_actions then go_up
    when *@down_actions then go_down
    when 'q', "\u0004" then throw :quit # ctrl-d
    else footer.message = "Unknown key: #{key.inspect}"
    end
  end

  def selected_line
    lines[selection]
  end

  ARROWS = {'A' => :arrow_up, 'B' => :arrow_down, 'C' => :arrow_right, 'D' => :arrow_left}.freeze

  def react
    key = case char = STDIN.getch
    when "\u0003" then exit 1
    when "\e"
      case subchar = STDIN.getch
      when '['
        ARROWS.fetch(STDIN.getch) {|k| "\e[#{k}" }
      else "\e#{subchar}"
      end
    else char
    end
    key_press key
  end

  def go_up
    footer.message = ''
    return if selection.zero?

    self.selection -= 1
  end

  def go_down
    footer.message = ''
    return if selection >= lines.size - 1

    self.selection += 1
  end

  def read_full_line message
    puts message
    gets
  end

  private

  def print_lines rows, columns
    used_lines = 0
    offset = [selection - 5, 0].max
    lines[offset..-1].map(&:to_s).each.with_index do |line, i|
      selected = i + offset == selection
      used_lines += height line, columns
      if used_lines > rows
        renderer.line '...', selected: selected
        break
      end

      renderer.line line, selected: selected
      break if used_lines == rows
    end

    fill_remaining_lines rows, used_lines
  end

  def fill_remaining_lines rows, used_lines
    (rows - used_lines).times { renderer.line '~', selected: false }
  end

  def height text, columns
    (text.size.to_f / columns).ceil
  end

  def required_lines text, columns
    text.split($INPUT_RECORD_SEPARATOR)
      .inject(0) {|sum, line| sum + height(line, columns) }
  end

  def clear
    system(Gem.win_platform? ? 'cls' : 'clear')
  end
end
