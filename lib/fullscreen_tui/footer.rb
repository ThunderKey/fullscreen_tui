# frozen_string_literal: true

class FullscreenTui
  class Footer
    attr_accessor :message
    SEPERATOR = ' | '

    def initialize
      self.message = ''
    end

    def output width:, lines:, current_line:
      parts = [
        message,
        "#{(current_line + 1).to_s.rjust lines.to_s.size}/#{lines}",
      ]
      part_width = width - parts[1].size - SEPERATOR.size
      parts[0] = if parts[0].size > part_width
        "#{parts[0][0...part_width - 3]}..."
      else
        parts[0].ljust part_width
      end
      parts.join SEPERATOR
    end
  end
end
