# frozen_string_literal: true

class FullscreenTui
  class Header
    attr_accessor :parts
    SEPERATOR = $INPUT_RECORD_SEPARATOR

    def initialize parts = nil
      self.parts = parts.nil? ? [] : parts
    end

    def output
      parts.join SEPERATOR
    end
  end
end
