# frozen_string_literal: true

require 'colorize'

class FullscreenTui
  class Renderer
    def header line
      puts line
    end

    def line line, selected:
      puts(selected ? line.underline : line)
    end

    def footer line
      print line
    end
  end
end
